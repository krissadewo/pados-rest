package com.dailycode.pados.model.dao;

import com.dailycode.pados.model.entity.Book;
import java.util.List;
import org.springframework.dao.DataAccessException;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public interface BookDao {

    List<Book> getByTitle(String title) throws DataAccessException;

    Book getById(Long id) throws DataAccessException;

    List<Book> getNewBook() throws DataAccessException;
}
