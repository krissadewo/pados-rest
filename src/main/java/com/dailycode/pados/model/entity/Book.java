package com.dailycode.pados.model.entity;

import java.util.Date;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class Book {

    private Long id;
    private String code;
    private String title;
    private String isbn;
    private String writer;
    private String publisher;
    private Date publishDate;
    private Short numberOfPage = 1;
    private Short numberOfBook = 1;
    private String synposis;
    private BookStatus bookStatus;
    private Date lastModified;
    private Date createDate;
    private Location location;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String isbn) {
        this.isbn = isbn;
    }

    public String getWriter() {
        return writer;
    }

    public void setWriter(String writer) {
        this.writer = writer;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public Date getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(Date publishDate) {
        this.publishDate = publishDate;
    }

    public Short getNumberOfPage() {
        return numberOfPage;
    }

    public void setNumberOfPage(Short numberOfPage) {
        this.numberOfPage = numberOfPage;
    }

    public Short getNumberOfBook() {
        return numberOfBook;
    }

    public void setNumberOfBook(Short numberOfBook) {
        this.numberOfBook = numberOfBook;
    }

    public String getSynposis() {
        return synposis;
    }

    public void setSynposis(String synposis) {
        this.synposis = synposis;
    }

    public BookStatus getBookStatus() {
        return bookStatus;
    }

    public void setBookStatus(BookStatus bookStatus) {
        this.bookStatus = bookStatus;
    }

    public Date getLastModified() {
        return lastModified;
    }

    public void setLastModified(Date lastModified) {
        this.lastModified = lastModified;
    }

    public Date getCreateDate() {
        return createDate;
    }

    public void setCreateDate(Date createDate) {
        this.createDate = createDate;
    }

    public Location getLocation() {
        return location;
    }

    public void setLocation(Location location) {
        this.location = location;
    }
}
