package com.dailycode.pados.model.entity;

/**
 *
 * @author kris
 */
public enum BookStatus {

    ALL, AVAILABLE, RETAIN, LOOSE, UNKNOWN;
}
