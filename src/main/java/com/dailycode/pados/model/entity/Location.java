package com.dailycode.pados.model.entity;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
public class Location {

    private Integer id;
    private String name;

    public Location(Integer id, String name) {
        this.id = id;
        this.name = name;
    }

    public Location() {
    }

    public Location(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Location{" + "id=" + id + ", name=" + name + '}';
    }
}
