package com.dailycode.pados.service.dao;

import com.dailycode.pados.model.dao.BookDao;
import com.dailycode.pados.model.entity.Book;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Kris Sadewo <krissadewo@ossys.com>
 */
@Repository
public class BookService {

    @Autowired
    private BookDao bookDao;
    private static final Logger LOGGER = LoggerFactory.getLogger(BookService.class);

    public Book getById(Long book) {
        try {
            return bookDao.getById(book);
        } catch (DataAccessException ex) {
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    public List<Book> getNewBook() {
        try {
            return bookDao.getNewBook();
        } catch (DataAccessException ex) {
            LOGGER.error(ex.getMessage());
        }
        return null;
    }

    public List<Book> getByTitle(String title) {
        try {
            return bookDao.getByTitle(title);
        } catch (DataAccessException ex) {
            LOGGER.error(ex.getMessage());
        }
        return null;
    }
}
