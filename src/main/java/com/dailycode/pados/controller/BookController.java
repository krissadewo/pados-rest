package com.dailycode.pados.controller;

import com.dailycode.pados.model.entity.Book;
import com.dailycode.pados.service.dao.BookService;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/book")
public class BookController {

    @Autowired
    @Qualifier(value = "bookService")
    private BookService bookService;

    @RequestMapping(value = "/getNewBook", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Book> getNewBook() {
        return bookService.getNewBook();
    }

    @RequestMapping(value = "/id/{id}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    Book getBookById(@PathVariable Long id) {
        return bookService.getById(id);
    }

    @RequestMapping(value = "/title/{title}", method = RequestMethod.GET, produces = "application/json")
    public @ResponseBody
    List<Book> getBookByTitle(@PathVariable String title) {
        return bookService.getByTitle("%" + title + "%");
    }
}